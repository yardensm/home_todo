<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{ 
        //database fields that are allowed for massive assignment
    protected $fillable = [
        'title',
        'status',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
 
}
