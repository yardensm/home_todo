<h1>This is your book list</h1>

<table border="4">
<thead>
    <tr>
        <th>Title</th>
        <th>Author</th>
    <tr>
    </thead>   
    @foreach($books as $book)
    <tr>
        
        <td>{{$book->title}}</td>
        
        <td>{{$book->author}}</td>
    </tr>
    @endforeach
</table> 