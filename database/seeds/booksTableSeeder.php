<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'title' => 'Buy milk',
            'author' => 'Avi Malichi',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'prepare for the test',
            'author' => 'David Dor',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'Read a book',
            'author' => 'Yarden Muallem',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'Hello World',
            'author' => 'Roni Horowitz',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'Reading a Book',
            'author' => 'Yarden Muallem',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'Hello Hello',
            'author' => 'Roni Horowitz',
            'created_at' => date('Y-m-d G:i:s'),
        ]
        ]
        );  
        
    }
}